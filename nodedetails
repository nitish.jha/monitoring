{
  "annotations": {
    "list": [
      {
        "builtIn": 1,
        "datasource": "-- Grafana --",
        "enable": true,
        "hide": true,
        "iconColor": "rgba(0, 211, 255, 1)",
        "name": "Annotations & Alerts",
        "target": {
          "limit": 100,
          "matchAny": false,
          "tags": [],
          "type": "dashboard"
        },
        "type": "dashboard"
      }
    ]
  },
  "editable": true,
  "gnetId": null,
  "graphTooltip": 0,
  "id": 4,
  "links": [],
  "panels": [
    {
      "datasource": null,
      "description": "node memory usage",
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "custom": {
            "axisPlacement": "auto",
            "axisSoftMax": -4,
            "barAlignment": 0,
            "drawStyle": "line",
            "fillOpacity": 9,
            "gradientMode": "none",
            "hideFrom": {
              "legend": false,
              "tooltip": false,
              "viz": false
            },
            "lineInterpolation": "linear",
            "lineStyle": {
              "fill": "solid"
            },
            "lineWidth": 2,
            "pointSize": 5,
            "scaleDistribution": {
              "type": "linear"
            },
            "showPoints": "auto",
            "spanNulls": false,
            "stacking": {
              "group": "A",
              "mode": "none"
            },
            "thresholdsStyle": {
              "mode": "off"
            }
          },
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "purple",
                "value": null
              },
              {
                "color": "orange",
                "value": 80
              }
            ]
          },
          "unit": "bytes"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 0,
        "y": 0
      },
      "id": 10,
      "options": {
        "legend": {
          "calcs": [],
          "displayMode": "list",
          "placement": "bottom"
        },
        "tooltip": {
          "mode": "single"
        }
      },
      "pluginVersion": "8.1.3",
      "targets": [
        {
          "exemplar": true,
          "expr": "node_memory_MemTotal_bytes{instance=\"3.144.125.52:9100\",job=\"node\"} - node_memory_MemFree_bytes{instance=\"3.144.125.52:9100\",job=\"node\"} - node_memory_Cached_bytes{instance=\"3.144.125.52:9100\",job=\"node\"} - node_memory_Buffers_bytes{instance=\"3.144.125.52:9100\",job=\"node\"}",
          "format": "time_series",
          "interval": "",
          "legendFormat": "Used",
          "refId": "A"
        }
      ],
      "title": "Memory Usage",
      "type": "timeseries"
    },
    {
      "datasource": null,
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "custom": {
            "axisLabel": "",
            "axisPlacement": "auto",
            "barAlignment": 0,
            "drawStyle": "line",
            "fillOpacity": 0,
            "gradientMode": "none",
            "hideFrom": {
              "legend": false,
              "tooltip": false,
              "viz": false
            },
            "lineInterpolation": "linear",
            "lineStyle": {
              "fill": "solid"
            },
            "lineWidth": 1,
            "pointSize": 1,
            "scaleDistribution": {
              "type": "linear"
            },
            "showPoints": "auto",
            "spanNulls": false,
            "stacking": {
              "group": "A",
              "mode": "none"
            },
            "thresholdsStyle": {
              "mode": "off"
            }
          },
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "yellow",
                "value": null
              },
              {
                "color": "yellow",
                "value": 80
              }
            ]
          },
          "unit": "percent"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 12,
        "y": 0
      },
      "id": 6,
      "options": {
        "legend": {
          "calcs": [],
          "displayMode": "list",
          "placement": "bottom"
        },
        "tooltip": {
          "mode": "single"
        }
      },
      "pluginVersion": "8.1.3",
      "targets": [
        {
          "exemplar": true,
          "expr": "irate(node_cpu_seconds_total{cpu=\"0\",job=\"node\",mode!=\"idle\"}[5m])",
          "interval": "",
          "legendFormat": "{{mode}}",
          "refId": "A"
        }
      ],
      "title": "CPU utilization",
      "type": "timeseries"
    },
    {
      "datasource": null,
      "description": "Network received and transmit",
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "custom": {
            "axisLabel": "",
            "axisPlacement": "auto",
            "barAlignment": 0,
            "drawStyle": "line",
            "fillOpacity": 0,
            "gradientMode": "none",
            "hideFrom": {
              "legend": false,
              "tooltip": false,
              "viz": false
            },
            "lineInterpolation": "linear",
            "lineWidth": 1,
            "pointSize": 5,
            "scaleDistribution": {
              "type": "linear"
            },
            "showPoints": "auto",
            "spanNulls": false,
            "stacking": {
              "group": "A",
              "mode": "none"
            },
            "thresholdsStyle": {
              "mode": "off"
            }
          },
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "green",
                "value": null
              },
              {
                "color": "red",
                "value": 80
              }
            ]
          },
          "unit": "KBs"
        },
        "overrides": []
      },
      "gridPos": {
        "h": 8,
        "w": 11,
        "x": 0,
        "y": 8
      },
      "id": 8,
      "options": {
        "legend": {
          "calcs": [],
          "displayMode": "list",
          "placement": "bottom"
        },
        "tooltip": {
          "mode": "single"
        }
      },
      "targets": [
        {
          "exemplar": true,
          "expr": "irate(node_network_receive_bytes_total{ instance=\"3.144.125.52:9100\",job=\"node\"}[5m])",
          "interval": "",
          "legendFormat": "{{device}}receive",
          "refId": "A"
        },
        {
          "exemplar": true,
          "expr": "irate(node_network_transmit_bytes_total{ instance=\"3.144.125.52:9100\",job=\"node\"}[5m])",
          "hide": false,
          "interval": "",
          "legendFormat": "{{device}}transmit",
          "refId": "B"
        }
      ],
      "title": "Network traffic",
      "type": "timeseries"
    },
    {
      "datasource": null,
      "description": "device=\"/dev/root\nfstype=\"ext4\"",
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "dark-yellow",
                "value": null
              },
              {
                "color": "#373210",
                "value": 80
              }
            ]
          }
        },
        "overrides": []
      },
      "gridPos": {
        "h": 8,
        "w": 7,
        "x": 11,
        "y": 8
      },
      "id": 2,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [],
          "fields": "",
          "values": false
        },
        "text": {},
        "textMode": "auto"
      },
      "pluginVersion": "8.1.3",
      "targets": [
        {
          "exemplar": true,
          "expr": "ceil(node_filesystem_size_bytes{device=\"/dev/root\",job=\"node\"}/(1024*1024))/1024",
          "interval": "",
          "legendFormat": "Total GB",
          "refId": "A"
        },
        {
          "exemplar": true,
          "expr": "ceil(node_filesystem_avail_bytes{device=\"/dev/root\",job=\"node\"}/(1024*1024))/1024",
          "hide": false,
          "interval": "",
          "intervalFactor": 1,
          "legendFormat": "Available GB",
          "refId": "B"
        },
        {
          "exemplar": true,
          "expr": "ceil(node_filesystem_size_bytes{device=\"/dev/root\"}/(1024*1024))/1024-ceil(node_filesystem_avail_bytes{device=\"/dev/root\",job=\"node\"}/(1024*1024))/1024",
          "format": "time_series",
          "hide": false,
          "instant": false,
          "interval": "",
          "legendFormat": "Used GB",
          "refId": "C"
        }
      ],
      "title": "FILESYSTEM",
      "type": "stat"
    },
    {
      "datasource": null,
      "description": "Node Memory info",
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "green",
                "value": null
              },
              {
                "color": "red",
                "value": 80
              }
            ]
          }
        },
        "overrides": []
      },
      "gridPos": {
        "h": 7,
        "w": 6,
        "x": 18,
        "y": 8
      },
      "id": 4,
      "options": {
        "colorMode": "value",
        "graphMode": "area",
        "justifyMode": "auto",
        "orientation": "auto",
        "reduceOptions": {
          "calcs": [
            "lastNotNull"
          ],
          "fields": "",
          "values": false
        },
        "text": {},
        "textMode": "auto"
      },
      "pluginVersion": "8.1.3",
      "targets": [
        {
          "exemplar": true,
          "expr": "ceil(node_memory_MemTotal_bytes{instance=\"3.144.125.52:9100\",job=\"node\"}/(1024*1024))/1024",
          "interval": "",
          "legendFormat": "Total GB",
          "refId": "A"
        },
        {
          "exemplar": true,
          "expr": "ceil(node_memory_MemFree_bytes{instance=\"3.144.125.52:9100\",job=\"node\"}/(1024*1024))/1024",
          "hide": false,
          "interval": "",
          "legendFormat": "Available GB",
          "refId": "B"
        },
        {
          "exemplar": true,
          "expr": "(ceil(node_memory_MemTotal_bytes{instance=\"3.144.125.52:9100\",job=\"node\"}/(1024*1024))/1024)-(ceil(node_memory_MemFree_bytes{instance=\"3.144.125.52:9100\",job=\"node\"})/(1024*1024))/1024",
          "hide": false,
          "interval": "",
          "legendFormat": "Used GB",
          "refId": "C"
        }
      ],
      "title": "RAM info",
      "type": "stat"
    }
  ],
  "schemaVersion": 30,
  "style": "dark",
  "tags": [],
  "templating": {
    "list": []
  },
  "time": {
    "from": "now-1h",
    "to": "now"
  },
  "timepicker": {},
  "timezone": "",
  "title": "Node and squid Dashboard",
  "uid": "5JaBQyS7z",
  "version": 14
}
